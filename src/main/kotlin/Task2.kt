import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main(){
    seqCall()
    asyncCall()
}

private fun seqCall() {
    val startTime = System.nanoTime()
    runBlocking {
        val a = first()
        val b = second()
        println("Sum = ${a + b}")
    }
    println("Time = ${System.nanoTime() - startTime}")
}

private fun asyncCall() {
    val startTime = System.nanoTime()
    runBlocking {
        val a = async {  first() }
        val b = async {  second() }
        println("Sum = ${a.await() + b.await()}")
    }
    println("Time async = ${System.nanoTime() - startTime}")
}

suspend fun first(): Int {
    delay(1000L)
    return 2
}

suspend fun second(): Int {
    delay(1000L)
    return 3
}

