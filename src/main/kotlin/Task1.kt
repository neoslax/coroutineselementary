import kotlinx.coroutines.*

fun main(a: Array<String>) {
    runBlocking {
        launch {
            printWorld()
        }
        printHello()
    }
}

suspend fun printWorld() {
    delay(1000L)
    println("World")
}

fun printHello() {
    Thread.sleep(2000L)
    println("Hello, ")
}